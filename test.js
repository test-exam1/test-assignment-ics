const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const fs = require('fs');
const text = require('body-parser/lib/types/text');
let app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }))

//เชื่อมต่อ DB
const con = mysql.createConnection({
    host: 'localhost',
    user: "root",
    password: "",
    database: "test"
})
con.connect((err) => {
    if (err) {
        return console.error('error: ' + err.message);
    }
    console.log('Connection connecting to DB Succeed');
})

app.get('/', (req, res) => {
    res.writeHead(200, { 'content-type': 'text/plain' });
    res.end('Hello Word');
})

//แสดงสินค้า
app.get('/product', (req, res, next) => {
    const search = req.query;
    con.query('SELECT * FROM `product`', (err, rows) => {
        if (err) console.log(err);
        console.log(rows);
        const filte = rows.filter(pro => {
            let isValid = true;
            for (key in search) {
                //console.log(key, pro[key], search[key]);
                isValid = isValid && pro[key] == search[key];
            }
            return isValid;
        });
        res.send(filte);
        //res.end(JSON.stringify(rows));
    })
})

//เพิ่มสินค้าใหม่
app.post('/newproduct', (req, res) => {
    let proname = req.body.proname;
    let num = req.body.num;
    let size = req.body.size;
    let color = req.body.color;
    let pattern = req.body.pattern;
    let price = req.body.price;
    let gender = req.body.gender;

    var product = {
        product_name: proname,
        product_num: num,
        product_size: size,
        product_color: color,
        product_pattern: pattern,
        product_price: price,
        gender: gender
    }

    con.query('INSERT INTO `product` SET ?', product, (err, newPro) => {
        if (err) {
            return console.error(err.message);
        }
        console.log('Rows affected:', newPro.affectedRows);
        res.status(201).json({
            message: 'add product 1 insert'
        })
    })
})

//อัพเดต product
app.post('/updateProduct:id', (req, res) => {
    var { id } = req.params;
    let id_pro = id;
    let proname = req.body.proname;
    let num = req.body.num;
    let price = req.body.price;

    let data = [proname, num, price, id_pro]

    con.query('UPDATE `product` SET `product_name`= ? ,`product_num`= ? ,`product_price`= ?  WHERE product_id = ? ', data, (err, results) => {
        if (err) {
            return console.error(err.message);
        }
        console.log('Rows affected:', results.affectedRows);
        res.status(201).json({
            message: 'update product 1 insert'
        })
    })
})

//ลบ product
app.delete('/delproduct:id', (req, res) => {
    var { id } = req.params;
    let id_pro = id;

    con.query('DELETE FROM `product` WHERE product_id = ?', [id_pro], (err, rows) => {
        if (err) {
            return console.error(err.message);
        }
        console.log('Rows affected:', rows.affectedRows);
        res.status(201).json({
            message: 'del 1 product'
        })
    })
})

//สั่งสินค้า
app.post('/neworder', (req, res) => {
    let ref = req.body.ref;
    let proId = req.body.proid;
    let proPrice = req.body.proPrice;
    let proNum = req.body.proNum;
    let address = req.body.address;

    var order = {
        ref_id: ref,
        product_id: proId,
        pro_Price: proPrice,
        pro_Num: proNum,
        date: new Date(),
        paystatus: 'ยังไม่ชำระเงิน',
        statusnow: 'ยังไม่ชำระเงิน',
        address: address
    }

    con.query('INSERT INTO `order` SET ?', order, (err, newRow) => {
        if (err) {
            return console.error(err.message);
        }
        console.log('Rows affected:', newRow.affectedRows);
        res.status(201).json({
            message: 'add order 1 insert'
        })
    })
})

//อัพเดต order
app.post('/updateorder:id', (req, res) => {
    var { id } = req.params;
    let id_order = id;
    let pay = 'ชำระเงินแล้ว';
    let now = req.body.status;
    let address = req.body.address;

    let data = [pay, now, address, id_order]

    con.query('UPDATE `order` SET `paystatus`= ? ,`statusnow`= ? ,`address`= ?  WHERE order_id = ? ', data, (err, results) => {
        if (err) {
            return console.error(err.message);
        }
        console.log('Rows affected:', results.affectedRows);
        res.status(201).json({
            message: 'update order 1 insert'
        })
    })
})

//ลบ order
app.delete('/delorder:id', (req, res) => {
    var { id } = req.params;
    let id_order = id;

    con.query('DELETE FROM `order` WHERE order_id = ?', [id_order], (err, rows) => {
        if (err) {
            return console.error(err.message);
        }
        console.log('Rows affected:', rows.affectedRows);
        res.status(201).json({
            message: 'del 1 order'
        })
    })
})

//แสดง order
app.get('/order', (req, res, next) => {
    const search = req.query;
    con.query('SELECT * FROM `order`', (err, rows) => {
        if (err) console.log(err);
        console.log(rows);
        const filte = rows.filter(order => {
            let isValid = true;
            for (key in search) {
                isValid = isValid && order[key] == search[key];
            }
            return isValid;
        });
        res.send(filte);
    })
})

app.listen(8081, '127.0.0.1', function() {
    console.log('Server Listen at http://127.0.0.1:8081');
});